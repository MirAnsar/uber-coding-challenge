//
//  FlickrImagesView.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 17/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import UIKit

protocol FlickrImagesViewDelegate: class {
    func flickrImagesView(_ view: FlickrImagesView, loadMoreImages page: Int, searchTerm: String)
}

class FlickrImagesView: UIView {
    
    weak var delegate: FlickrImagesViewDelegate?
    @IBOutlet weak var collectionView: UICollectionView!
    var searchTerm = ""
    var totalPages = Int()
    var page = Int()
    var flickrImages = [FlickrPhoto]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    private let flickrImageViewCellReuseIdentifier = String(describing: FlickrImageViewCell.self)
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupSubViews()
    }
    
    // Setup all subViews
    private func setupSubViews() {
        setupCollectionView()
        registerCollectionViewCells()
    }
    
    // Setup collection view
    private func setupCollectionView() {
        addSubview(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        // Setup collection view constraints
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topAnchor, constant: 0), collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0), collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0), collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0)
            ])
    }
    
    // Register collection view cell
    private func registerCollectionViewCells() {
        collectionView.register(FlickrImageViewCell.self, forCellWithReuseIdentifier: flickrImageViewCellReuseIdentifier)
    }
}

// Collection view delegate and datasource methods.
extension FlickrImagesView: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return flickrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: flickrImageViewCellReuseIdentifier, for: indexPath) as? FlickrImageViewCell else {
            return UICollectionViewCell()
        }
        // Configure cell with FlickrPhoto model
        cell.configureCell(photo: flickrImages[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == flickrImages.count - 1 {
            if page < totalPages {
                delegate?.flickrImagesView(self, loadMoreImages: page + 1, searchTerm: searchTerm)
            }
        }
    }
}

// Delegate methods for flow layout
extension FlickrImagesView: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width / 3.0
        let height = width
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
}
