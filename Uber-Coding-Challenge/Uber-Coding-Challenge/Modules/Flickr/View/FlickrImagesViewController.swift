//
//  FlickrImagesViewController.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 17/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import UIKit

class FlickrImagesViewController: UIViewController, DispatchTaskPerformer {
    
    @IBOutlet var flickrImagesView: FlickrImagesView!
    
    private let flickrImagesManager = FlickrImagesManager()
    private var searchText = "All"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationController()
        flickrImagesView.delegate = self
        getPhotos(text: searchText, page: 1)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        invalidateImagesCache()
    }
    
    // Set up navigation controller with search
    private func setupNavigationController() {
        navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationItem.largeTitleDisplayMode = .never
        let search = UISearchController(searchResultsController: nil)
        search.searchResultsUpdater = self
        self.navigationItem.searchController = search
        search.searchBar.placeholder = "Search Anything"
        title = "Flickr Images"
    }
    
    // Get photos from API via manager
    private func getPhotos(text: String, page: Int) {
        title = text.capitalized
        self.flickrImagesManager.getImages(text: text, page: page) { (images) in
            guard let photos = images.photo,
                let _totalPages = images.pages,
                let _page = images.page else {
                return
            }
            
            // Update view on main thread
            self.performTaskOnMainThread {
                self.flickrImagesView.flickrImages = photos
                
            }
            self.flickrImagesView.searchTerm = text
            self.flickrImagesView.totalPages = _totalPages
            self.flickrImagesView.page = _page
        }
    }
    
    // Add photos to view by appending the data received from the API via manager
    private func addPhotos(text: String, page: Int) {
        title = text.capitalized
        self.flickrImagesManager.getImages(text: text, page: page) { (images) in
            guard let photos = images.photo,
                let _totalPages = images.pages,
                let _page = images.page else {
                    return
            }
            self.performTaskOnMainThread {
                photos.forEach({ (photo) in
                    self.flickrImagesView.flickrImages.append(photo)
                })
                self.flickrImagesView.collectionView.reloadData()
            }
            self.flickrImagesView.searchTerm = text
            self.flickrImagesView.totalPages = _totalPages
            self.flickrImagesView.page = _page
        }
    }
    
    private func invalidateImagesCache() {
        FlickrCache.images.removeAllObjects()
    }
}

// Delegate method for search results updating.
extension FlickrImagesViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let _searchText = searchController.searchBar.text else {
            return
        }
        if _searchText.count > 0 {
            title = _searchText.capitalized
            searchText = _searchText
            getPhotos(text: _searchText, page: 1)
        }
    }
}

// Delegate method for flickr images view
extension FlickrImagesViewController: FlickrImagesViewDelegate {
    
    // This delegate is called as network calls are only made via manager from the view controller.
    func flickrImagesView(_ view: FlickrImagesView, loadMoreImages page: Int, searchTerm: String) {
        addPhotos(text: searchTerm, page: page)
    }
}
