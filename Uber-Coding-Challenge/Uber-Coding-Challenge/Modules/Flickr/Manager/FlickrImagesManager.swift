//
//  FlickrImagesManager.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 17/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import Foundation

struct FlickrImagesManager {
    
    let flickrService = FlickrService()
    
    func getImages(text: String, page: Int,  completion: @escaping (FlickrPhotos) -> Void) {
        flickrService.getFlickrImages(searchText: text, page: page) { (data, error) in
            guard let photos = data else {
                return
            }
            completion(photos)
        }
    }
}
