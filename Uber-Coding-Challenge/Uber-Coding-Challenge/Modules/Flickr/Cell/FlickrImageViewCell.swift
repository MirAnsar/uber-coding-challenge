//
//  FlickrImageViewCell.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 21/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import UIKit

class FlickrImageViewCell: UICollectionViewCell {
    
    let imageView = UIImageView()
    let loader = UIActivityIndicatorView()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
    
    private func setupImageView() {
        addSubview(imageView)
        addSubview(loader)
        setupLoader()
        imageView.contentMode = .scaleToFill
        imageView.bounds = bounds
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0)
            ])
    }
    
    private func setupLoader() {
        loader.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        bringSubviewToFront(loader)
        loader.translatesAutoresizingMaskIntoConstraints = false
        loader.hidesWhenStopped = true
        loader.color = UIColor.darkGray
        loader.style = .gray
        
        NSLayoutConstraint.activate([
            loader.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0),
            loader.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0)
            ])
    }
    
    func configureCell(photo: FlickrPhoto) {
        loader.startAnimating()
        setupImageView()
        guard let url = photo.photoURL() as NSURL? else  {
            return
        }
        
        if let cachedImageData = FlickrCache.images.object(forKey: url) {
            imageView.image = UIImage(data: cachedImageData as Data)
            loader.stopAnimating()
        } else {
            imageView.loadFlickrImageFromURL(photo)
            loader.stopAnimating()
        }
    }
    
}
