//
//  FlickrService.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 17/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import Foundation

class FlickrService: APIService {
    
    func getFlickrImages(searchText: String, page: Int, completion: @escaping(_ result: FlickrPhotos?, _ error: Error?) -> Void) {
        let params: [String: String] = ["page":"\(page)", "text":"\(searchText)"]
        makeRequest(URN: GetImagesURN.self, parameters: params) { (result) in
            switch result {
            case .success(let data):
                completion(data.photos, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
