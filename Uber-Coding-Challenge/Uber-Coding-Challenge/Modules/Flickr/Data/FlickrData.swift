//
//  FlickrData.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 21/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import Foundation

struct FlickrCache {
    static let images = NSCache<NSURL, NSData>()
}

struct FlickrData : Codable {
    
    var photos : FlickrPhotos?
    let stat : String?
    
    enum CodingKeys: String, CodingKey {
        case photos = "photos"
        case stat = "stat"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        photos = try FlickrPhotos(from: decoder)
        stat = try values.decodeIfPresent(String.self, forKey: .stat)
        if let _photos = try? values.decode(FlickrPhotos.self, forKey: .photos) {
            photos = _photos
        }
    }
    
}

struct FlickrPhotos : Codable {
    
    let page : Int?
    let pages : Int?
    let perpage : Int?
    var photo : [FlickrPhoto]?
    let total : String?
    
    enum CodingKeys: String, CodingKey {
        case page = "page"
        case pages = "pages"
        case perpage = "perpage"
        case photo = "photo"
        case total = "total"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        page = try values.decodeIfPresent(Int.self, forKey: .page)
        pages = try values.decodeIfPresent(Int.self, forKey: .pages)
        perpage = try values.decodeIfPresent(Int.self, forKey: .perpage)
        photo = try values.decodeIfPresent([FlickrPhoto].self, forKey: .photo)
        total = try values.decodeIfPresent(String.self, forKey: .total)
        if let _photo = try? values.decode([FlickrPhoto].self, forKey: .photo) {
            photo = _photo
        }
    }
    
}

struct FlickrPhoto : Codable {
    
    let farm : Int?
    let id : String?
    let isfamily : Int?
    let isfriend : Int?
    let ispublic : Int?
    let owner : String?
    let secret : String?
    let server : String?
    let title : String?
    
    enum CodingKeys: String, CodingKey {
        case farm = "farm"
        case id = "id"
        case isfamily = "isfamily"
        case isfriend = "isfriend"
        case ispublic = "ispublic"
        case owner = "owner"
        case secret = "secret"
        case server = "server"
        case title = "title"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        farm = try values.decodeIfPresent(Int.self, forKey: .farm)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        isfamily = try values.decodeIfPresent(Int.self, forKey: .isfamily)
        isfriend = try values.decodeIfPresent(Int.self, forKey: .isfriend)
        ispublic = try values.decodeIfPresent(Int.self, forKey: .ispublic)
        owner = try values.decodeIfPresent(String.self, forKey: .owner)
        secret = try values.decodeIfPresent(String.self, forKey: .secret)
        server = try values.decodeIfPresent(String.self, forKey: .server)
        title = try values.decodeIfPresent(String.self, forKey: .title)
    }
    
    func photoURL() -> URL? {
        guard let _farm = farm,
            let _server = server,
            let _id = id,
            let _secret = secret,
            let url = URL(string: "http://farm\(_farm).static.flickr.com/\(_server)/\(_id)_\(_secret).jpg")
            else {
                return nil
        }
        return url
    }
}
