//
//  URN.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 21/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import Foundation

protocol URN {
    associatedtype Derived: Codable
    static var urn: String { get }
    static var method: String { get }
}

protocol JSONEncodedURN: URN {}

struct GetImagesURN: JSONEncodedURN {
    typealias Derived = FlickrData
    static var urn: String { return FlickrURN.searchURL }
    static var method: String { return "GET" }
}
