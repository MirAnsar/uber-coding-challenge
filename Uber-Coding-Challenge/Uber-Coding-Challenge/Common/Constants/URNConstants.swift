//
//  URNConstants.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 21/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import Foundation

enum FlickrURN {
    static let searchURL = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736&format=json&nojsoncallback=1&safe_search=1"}
