//
//  DispatchTaskPerformer.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 22/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import Foundation

protocol DispatchTaskPerformer {
    func performTaskOnMainThread(_ task: @escaping ()->())
    func performTaskOnBackgroundThread(_ task: @escaping ()->())
}

extension DispatchTaskPerformer {
    
    func performTaskOnMainThread(_ task: @escaping ()->()) {
        DispatchQueue.main.async(execute: task)
    }
    
    func performTaskOnBackgroundThread(_ task: @escaping ()->()) {
        DispatchQueue.global(qos: .background).async(execute: task)
    }
}
