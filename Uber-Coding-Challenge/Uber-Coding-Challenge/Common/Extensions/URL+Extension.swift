//
//  URL+Extension.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 22/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import Foundation

extension URL {
    
    @discardableResult
    func addParameters(_ parameters: [String: String]) -> URL? {
        guard var urlComponents = URLComponents(string:  absoluteString) else {
            return absoluteURL
        }
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        
        parameters.forEach { (key, value) in
            let queryItem = URLQueryItem(name: key, value: value)
            queryItems.append(queryItem)
        }
        urlComponents.queryItems = queryItems
        guard let _url = urlComponents.url else {
            return nil
        }
        return _url
    }
}
