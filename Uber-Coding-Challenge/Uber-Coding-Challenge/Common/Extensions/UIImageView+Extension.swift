//
//  UIImageView+Extension.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 22/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import UIKit

extension UIImageView: DispatchTaskPerformer {
    
    func loadFlickrImageFromURL(_ photo: FlickrPhoto) {
        
        guard let url = photo.photoURL() else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                return
            }
            
            guard let _image = data else {
                return
            }
            FlickrCache.images.setObject(_image as NSData, forKey: url as NSURL)
            self.performTaskOnMainThread {
                self.image = UIImage(data: _image)
            }
        }.resume()
    }
}
