//
//  APIService.swift
//  Uber-Coding-Challenge
//
//  Created by Mir Ansar Ali Wasif on 17/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import Foundation

enum Result<T: Codable> {
    case success(T)
    case failure(Error)
}

class APIService {
    
    func makeRequest<T: URN>(URN: T.Type,
                             parameters: [String: String]? = nil,
                             completion: @escaping (Result<T.Derived>) -> Void) {
        
        guard let _url = URL(string: URN.urn),
            let _parameters = parameters else {
            return
        }
        guard let urlWithParameters = _url.addParameters(_parameters) else {
            return
        }
        var request = URLRequest(url: urlWithParameters)
        request.httpMethod = "GET"
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            
            guard let _data = data else {
                return
            }
            self.decodeResponseData(_data, completion: completion)
        })
        
        task.resume()
    }
    
    private func decodeResponseData<T>(_ data: Data, completion: @escaping (Result<T>) -> Void) {
        do {
            let decoder = JSONDecoder()
            let model = try decoder.decode(T.self, from:
                data)
            completion(.success(model))
        } catch let error {
            assertionFailure(error.localizedDescription)
        }
    }
}
