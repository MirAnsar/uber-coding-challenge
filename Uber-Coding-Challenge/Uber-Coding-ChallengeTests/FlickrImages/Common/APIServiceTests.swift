//
//  APIServiceTests.swift
//  Uber-Coding-ChallengeTests
//
//  Created by Mir Ansar Ali Wasif on 23/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import XCTest

@testable import Uber_Coding_Challenge
class APIServiceTests: XCTestCase {
    
    let apiService = APIService()
    
    func test_makeRequest() {
        apiService.makeRequest(URN: GetImagesURN.self) { (result) in
            XCTAssertNotNil(result, "Result should not be nil")
        }
    }
}
