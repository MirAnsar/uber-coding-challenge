//
//  FlickrImagesViewControllerTests.swift
//  Uber-Coding-ChallengeTests
//
//  Created by Mir Ansar Ali Wasif on 23/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import XCTest

@testable import Uber_Coding_Challenge
class FlickrImagesViewControllerTests: XCTestCase {
    
    var flickrImagesViewController: FlickrImagesViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Flickr", bundle: nil)
        
        let navigationController = storyboard.instantiateViewController(withIdentifier: "FlickrImagesNavigationController") as? UINavigationController
        
        flickrImagesViewController = navigationController?.topViewController as? FlickrImagesViewController
        
        _ = flickrImagesViewController.view
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_CanInstantiateViewController() {
        
        XCTAssertNotNil(flickrImagesViewController)
    }
    
    func test_HasSearchBar() {
        
        XCTAssertNotNil(flickrImagesViewController.navigationItem.searchController)
    }
    
    func test_ShouldSetSearchBarDelegate() {

        XCTAssertNotNil(flickrImagesViewController.navigationItem.searchController!.searchResultsUpdater)
    }
}
