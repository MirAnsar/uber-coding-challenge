//
//  FlickrImagesViewTests.swift
//  Uber-Coding-ChallengeTests
//
//  Created by Mir Ansar Ali Wasif on 23/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import XCTest

@testable import Uber_Coding_Challenge
class FlickrImagesViewTests: XCTestCase {
    
    var flickrImagesViewController: FlickrImagesViewController!
    let flickrImagesView = FlickrImagesView()
    
    override func setUp() {
        super.setUp()
        let storyboard: UIStoryboard = UIStoryboard(name: "Flickr", bundle: nil)
        
        let navigationController = storyboard.instantiateViewController(withIdentifier: "FlickrImagesNavigationController") as? UINavigationController
        
        flickrImagesViewController = navigationController?.topViewController as? FlickrImagesViewController
        flickrImagesViewController.loadView()
        flickrImagesViewController.viewDidLoad()
    }
    
    func test_ViewNotNil() {
        
        XCTAssertNotNil(flickrImagesView, "Flickr images view should not be nil")
    }
    
    func test_CollectionViewIsNotNilAfterViewDidLoad() {
        XCTAssertNotNil(flickrImagesViewController.flickrImagesView.collectionView)
    }
    
    func test_HasItemsForCollectionView() {
        XCTAssertNotNil(flickrImagesView.flickrImages)
    }
    
    func test_ShouldSetCollectionViewDataSource() {
        
        XCTAssertNotNil(flickrImagesViewController.flickrImagesView.collectionView.dataSource)
    }
    
    func test_ConformsToCollectionViewDataSource() {
        
        XCTAssert(flickrImagesViewController.flickrImagesView.conforms(to: UICollectionViewDataSource.self))
        XCTAssertTrue(flickrImagesViewController.flickrImagesView.responds(to: #selector(flickrImagesViewController.flickrImagesView.collectionView(_:numberOfItemsInSection:))))
        XCTAssertTrue(flickrImagesViewController.flickrImagesView.responds(to: #selector(flickrImagesViewController.flickrImagesView.collectionView(_:cellForItemAt:))))
    }
    
    func test_ShouldSetCollectionViewDelegate() {
        
        XCTAssertNotNil(flickrImagesViewController.flickrImagesView.collectionView.delegate)
    }
    
    func test_ConformsToCollectionViewDelegate() {
        
        XCTAssert(flickrImagesViewController.flickrImagesView.conforms(to: UICollectionViewDelegate.self))
    }
    
    func test_ConformsToCollectionViewDelegateFlowLayout () {
        
        XCTAssert(self.flickrImagesViewController.flickrImagesView.conforms(to: UICollectionViewDelegateFlowLayout.self))
        XCTAssertTrue(flickrImagesViewController.flickrImagesView.responds(to: #selector(flickrImagesViewController.flickrImagesView.collectionView(_:layout:sizeForItemAt:))))
        XCTAssertTrue(flickrImagesViewController.flickrImagesView.responds(to: #selector(flickrImagesViewController.flickrImagesView.collectionView(_:layout:insetForSectionAt:))))
        XCTAssertTrue(flickrImagesViewController.flickrImagesView.responds(to: #selector(flickrImagesViewController.flickrImagesView.collectionView(_:layout:minimumInteritemSpacingForSectionAt:))))
        XCTAssertTrue(flickrImagesViewController.flickrImagesView.responds(to: #selector(flickrImagesViewController.flickrImagesView.collectionView(_:layout:minimumLineSpacingForSectionAt:))))
    }
}

