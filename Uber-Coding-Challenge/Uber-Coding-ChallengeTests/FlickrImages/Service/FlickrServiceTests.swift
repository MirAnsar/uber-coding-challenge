//
//  FlickrServiceTests.swift
//  Uber-Coding-ChallengeTests
//
//  Created by Mir Ansar Ali Wasif on 23/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import XCTest

@testable import Uber_Coding_Challenge
class FlickrServiceTests: XCTestCase {
    
    let flickrService = FlickrService()

    func test_getImages_networkCall() {
        flickrService.getFlickrImages(searchText: "All", page: 1) { (photos, error) in
            XCTAssertNotNil(photos, "photos from response should not be nil")
        }
    }
}
