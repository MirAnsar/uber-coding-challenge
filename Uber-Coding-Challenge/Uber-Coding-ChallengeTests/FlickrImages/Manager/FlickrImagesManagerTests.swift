//
//  FlickrImagesManagerTests.swift
//  Uber-Coding-ChallengeTests
//
//  Created by Mir Ansar Ali Wasif on 23/10/18.
//  Copyright © 2018 miransar. All rights reserved.
//

import XCTest

@testable import Uber_Coding_Challenge
class FlickrImagesManagerTests: XCTestCase {

    let manager = FlickrImagesManager()
    
    func test_getImages() {
        manager.getImages(text: "All", page: 1) { (photos) in
            XCTAssertNotNil(photos, "Photos should not be nil")
        }
    }
}
