# Flickr Images App - Note For Reviewers

Hi Reviewers, this project is based on the task document shared and includes all the tasks mentioned along with the bonus tasks. Please read further to understand how the project is architected and why certain practices have been implemented.

## Architecture
I have used Viper style of Architecture to deliver the functionality for Flickr images, each project is a different use case and based on the design and architecture its scalability is measured.

I have also used Codable for parsing data as it makes things much easier.

```
Module - Each module contains of View, Manager, Service, and Data groups. 
View - Includes all view related classes and functionalities including storyboard.
Manager - Responsible for giving the view what it needs through network calls via any module service.
Service - Responsible for making api calls and providing the required data to the manager.
Data - Any model, cell provider, or codable object.
```

### UI

All the UI has been implemented programmatically including constraints, storyboards are easier but doing things programmatically makes thing clean and readable.

## Things Implemented

```
1 - Three column collection view with auto loading of images on scroll.
2 - Search controller in navigation item to support images search based on search input.
3 - Caching mechanism for better performance and less battery usage.
4 - Unit Testing.
```

## Things could've been implemented
```
1 - Better loading indicator.
2 - Better handling of view state while loading data.
3 - Better error handling.
```

## Cover

Honestly I spent around more than 4 hours as I worked on it in bits and pieces as and when I found out time. I started working on this project on 16th October, but later as I got more time before I deliver the task, I worked the rest part of it early today, that is Monday 22nd October. I hope you all like the code base and please do let me know the feedback. Thanks.

Regards |
Mir Ansar
